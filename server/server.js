//ทำการ import express เข้ามาใช้งาน โดยสร้างตัวแปร express ขึ้นมาเพื่อรับค่า
const express = require('express')
//ทำการสร้าง Instance ของ express และสร้างตัวแปร app ขึ้นมาเพื่อรับค่า
const app = express()
//สร้างตัวแปร PORT ขึ้นมารับค่า port ในกรณีที่เราได้กำหนดไว้ใน environment ของเครื่อง
//แต่ถ้าไม่ได้กำหนดไว้ เราจะใช้ค่า 8080 แทน
const PORT = process.env.PORT || 91
//สร้าง route ขึ้นมา 1 ตัว โดยกำหนดให้ path คือ / หรือ index ของ host นั่นเอง
//จากนั้นให้กำหนด response แสดงคำว่า Hello World

var mysql = require('mysql');

var con = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'motocyle'
});
// app.use(bodyParser.json());
app.get('/', (req, res) => res.send('Hello World'))
//run web server ที่เราสร้างไว้ โดยใช้ PORT ที่เรากำหนดไว้ในตัวแปร PORT

app.get('/tires',function(req,res){
    con.connect(function(err){
        if(err) throw err;
        console.log("Connected!");
        // sql = 'insert into tires (t_id,t_type,t_picture) values("11111","deto","pic1.jpg")';
        sql = 'select * from tires'
        con.query(sql,function(err,result){
            if (err) throw err;
            console.log("Select Complete");
            res.send(result)
        })
    })
})
app.get('/tires/:tid',function(req,res){
    con.connect(function(err){
        if(err) throw err;
        console.log("Connected!");
        // sql = 'insert into tires (t_id,t_type,t_picture) values("11111","deto","pic1.jpg")';
        sql = 'select * from tires where t_id = "'+req.params.tid+'"'
        con.query(sql,function(err,result){
            if (err) throw err;
            console.log("Select Complete");
            res.send(result)
        })
    })
})
app.listen(PORT, () => {
    //หากทำการ run server สำเร็จ ให้แสดงข้อความนี้ใน cmd หรือ terminal
    console.log(`Server is running on port : ${PORT}`)
})
//ทำการ export app ที่เราสร้างขึ้น เพื่อให้สามารถนำไปใช้งานใน project อื่นๆ 
//เปรียบเสมือนเป็น module ตัวนึง
module.exports = app